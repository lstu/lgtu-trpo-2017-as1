/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     19.05.2017 19:40:47                          */
/*==============================================================*/


DROP TABLE IF EXISTS ADDRES;

DROP TABLE IF EXISTS CATEGORY;

DROP TABLE IF EXISTS CATEGORY_SIZE;

DROP TABLE IF EXISTS PAYMENT_TYPE;

DROP TABLE IF EXISTS PICTURE;

DROP TABLE IF EXISTS POSITION_BASKET;

DROP TABLE IF EXISTS POSITION_PURCHASE;

DROP TABLE IF EXISTS PRICE_HISTORY;

DROP TABLE IF EXISTS PRODUCT;

DROP TABLE IF EXISTS TYPE;

DROP TABLE IF EXISTS USER;

DROP TABLE IF EXISTS FACT_SELLING;

DROP TABLE IF EXISTS PRODUCT_DETAIL;

DROP TABLE IF EXISTS PROVIDER;

DROP TABLE IF EXISTS PURCHASE;

DROP TABLE IF EXISTS SIZE;

DROP TABLE IF EXISTS �HARACTERISTICS;

/*==============================================================*/
/* Table: ADDRES                                                */
/*==============================================================*/
CREATE TABLE ADDRES
(
   ID_ADDRES            INT NOT NULL,
   ID_USER              INT NOT NULL,
   LAND_ADDRES          VARCHAR(500),
   CITY_ADDRES          VARCHAR(500),
   INDEX_ADDRES         INT,
   STREET_ADDRES        VARCHAR(1000),
   HOUSE_ADDRES         INT,
   FLAT_ADDRES          INT,
   PIC_KPOINT           VARCHAR(2000),
   PRIMARY KEY (ID_ADDRES)
);

/*==============================================================*/
/* Table: CATEGORY                                              */
/*==============================================================*/
CREATE TABLE CATEGORY
(
   ID_CATEGORY_DRESS    INT NOT NULL,
   ID_TYPE_DRESS        INT NOT NULL,
   CATEGORY_DRESS       VARCHAR(500) NOT NULL,
   USE_DRESS            VARCHAR(500) NOT NULL,
   PRIMARY KEY (ID_CATEGORY_DRESS)
);

/*==============================================================*/
/* Table: CATEGORY_SIZE                                         */
/*==============================================================*/
CREATE TABLE CATEGORY_SIZE
(
   ID_SIZE_CATEGORY     INT NOT NULL,
   TYPE_STANDART        VARCHAR(100) NOT NULL,
   PRIMARY KEY (ID_SIZE_CATEGORY)
);

/*==============================================================*/
/* Table: PAYMENT_TYPE                                          */
/*==============================================================*/
CREATE TABLE PAYMENT_TYPE
(
   ID_TYPE_PAYMENT      INT NOT NULL,
   ID_USER              INT NOT NULL,
   TYPE_PAYMENT         BOOL NOT NULL,
   PAYMENT_DETAILS      VARCHAR(2000) NOT NULL,
   PRIMARY KEY (ID_TYPE_PAYMENT)
);

/*==============================================================*/
/* Table: PICTURE                                               */
/*==============================================================*/
CREATE TABLE PICTURE
(
   FILE                 CHAR(10) NOT NULL,
   ID_PICTURE           CHAR(10) NOT NULL,
   PRODUCT_ARTICLE      CHAR(10),
   PRIMARY KEY (ID_PICTURE)
);

/*==============================================================*/
/* Table: POSITION_BASKET                                       */
/*==============================================================*/
CREATE TABLE POSITION_BASKET
(
   ID_BASKET            INT NOT NULL,
   PRODUCT_ARTICLE      CHAR(10) NOT NULL,
   ID_SALE              INT,
   DATA_BASKET          DATE NOT NULL,
   PRISE                INT NOT NULL,
   COUNT                INT NOT NULL,
   PRIMARY KEY (ID_BASKET)
);

/*==============================================================*/
/* Table: POSITION_PURCHASE                                     */
/*==============================================================*/
CREATE TABLE POSITION_PURCHASE
(
   ID_PURCHASE_POSITION INT NOT NULL,
   ID_PURCHASE          INT NOT NULL,
   PRODUCT_ARTICLE      CHAR(10),
   COMMENTS_PURCHASE    VARCHAR(100),
   PRICE_PURCHASE       INT NOT NULL,
   PRIMARY KEY (ID_PURCHASE_POSITION)
);

/*==============================================================*/
/* Table: PRICE_HISTORY                                         */
/*==============================================================*/
CREATE TABLE PRICE_HISTORY
(
   DATA_PRICE           DATE NOT NULL,
   ID_PRICE             INT NOT NULL,
   PRODUCT_ARTICLE      CHAR(10),
   PRICE                INT NOT NULL,
   PRIMARY KEY (ID_PRICE)
);

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
CREATE TABLE PRODUCT
(
   PRODUCT_NAME         VARCHAR(500),
   PRODUCT_ARTICLE      CHAR(10) NOT NULL,
   ID_�������           VARCHAR(100) NOT NULL,
   PRODUCT_BREND        VARCHAR(500),
   PRODUCT_DESCRIPTION  VARCHAR(1000),
   PRIMARY KEY (PRODUCT_ARTICLE)
);

/*==============================================================*/
/* Table: TYPE                                                  */
/*==============================================================*/
CREATE TABLE TYPE
(
   ID_TYPE_DRESS        INT NOT NULL,
   TYPE_DRESS           VARCHAR(500) NOT NULL,
   PRIMARY KEY (ID_TYPE_DRESS)
);

/*==============================================================*/
/* Table: USER                                                  */
/*==============================================================*/
CREATE TABLE USER
(
   USER_FIO             VARCHAR(50) NOT NULL,
   USER_ADRESS          VARCHAR(200) NOT NULL,
   USER_PHONE           CHAR(11) NOT NULL,
   USER_EMAIL           VARCHAR(50),
   ID_USER              INT NOT NULL,
   PRIMARY KEY (ID_USER)
);

/*==============================================================*/
/* Table: FACT_SELLING                                          */
/*==============================================================*/
CREATE TABLE FACT_SELLING
(
   ID_SALE              INT NOT NULL,
   ID_USER              INT NOT NULL,
   SALE_DATE            DATE NOT NULL,
   PRIMARY KEY (ID_SALE)
);

/*==============================================================*/
/* Table: PRODUCT_DETAIL                                        */
/*==============================================================*/
CREATE TABLE PRODUCT_DETAIL
(
   ID_PODUCT_DETAIL     INT NOT NULL,
   PRODUCT_ARTICLE      CHAR(10),
   ID_OPTION            INT NOT NULL,
   ID_TYPE_DRESS        INT NOT NULL,
   PRIMARY KEY (ID_PODUCT_DETAIL)
);

/*==============================================================*/
/* Table: PROVIDER                                              */
/*==============================================================*/
CREATE TABLE PROVIDER
(
   ID_PROVIDER          INT NOT NULL,
   PROVIDER_NAME        VARCHAR(500) NOT NULL,
   COUNTRY_PROVIDE      VARCHAR(100) NOT NULL,
   PRIMARY KEY (ID_PROVIDER)
);

/*==============================================================*/
/* Table: PURCHASE                                              */
/*==============================================================*/
CREATE TABLE PURCHASE
(
   ID_PURCHASE          INT NOT NULL,
   ID_PROVIDER          INT NOT NULL,
   PURCHASE_DATA        DATE NOT NULL,
   PRIMARY KEY (ID_PURCHASE)
);

/*==============================================================*/
/* Table: SIZE                                                  */
/*==============================================================*/
CREATE TABLE SIZE
(
   CLASSIFICATION       VARCHAR(100) NOT NULL,
   ID_�������           VARCHAR(100) NOT NULL,
   ID_SIZE_CATEGORY     INT NOT NULL,
   PRIMARY KEY (ID_�������)
);

/*==============================================================*/
/* Table: �HARACTERISTICS                                       */
/*==============================================================*/
CREATE TABLE �HARACTERISTICS
(
   ID_OPTION            INT NOT NULL,
   COLOR_PRODUCT        VARCHAR(100) NOT NULL,
   MATERIAL             VARCHAR(100) NOT NULL,
   DESCRIPTION_PRODUCT  VARCHAR(1000),
   PRIMARY KEY (ID_OPTION)
);

ALTER TABLE ADDRES ADD CONSTRAINT FK_RELATIONSHIP_18 FOREIGN KEY (ID_USER)
      REFERENCES USER (ID_USER) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE CATEGORY ADD CONSTRAINT FK_RELATIONSHIP_17 FOREIGN KEY (ID_TYPE_DRESS)
      REFERENCES TYPE (ID_TYPE_DRESS) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PAYMENT_TYPE ADD CONSTRAINT FK_RELATIONSHIP_19 FOREIGN KEY (ID_USER)
      REFERENCES USER (ID_USER) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PICTURE ADD CONSTRAINT FK_RELATIONSHIP_1 FOREIGN KEY (PRODUCT_ARTICLE)
      REFERENCES PRODUCT (PRODUCT_ARTICLE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE POSITION_BASKET ADD CONSTRAINT FK_RELATIONSHIP_15 FOREIGN KEY (ID_SALE)
      REFERENCES FACT_SELLING (ID_SALE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE POSITION_BASKET ADD CONSTRAINT FK_RELATIONSHIP_7 FOREIGN KEY (PRODUCT_ARTICLE)
      REFERENCES PRODUCT (PRODUCT_ARTICLE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE POSITION_PURCHASE ADD CONSTRAINT FK_RELATIONSHIP_13 FOREIGN KEY (ID_PURCHASE)
      REFERENCES PURCHASE (ID_PURCHASE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE POSITION_PURCHASE ADD CONSTRAINT FK_RELATIONSHIP_14 FOREIGN KEY (PRODUCT_ARTICLE)
      REFERENCES PRODUCT (PRODUCT_ARTICLE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PRICE_HISTORY ADD CONSTRAINT FK_RELATIONSHIP_10 FOREIGN KEY (PRODUCT_ARTICLE)
      REFERENCES PRODUCT (PRODUCT_ARTICLE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PRODUCT ADD CONSTRAINT FK_RELATIONSHIP_8 FOREIGN KEY (ID_�������)
      REFERENCES SIZE (ID_�������) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE FACT_SELLING ADD CONSTRAINT FK_RELATIONSHIP_20 FOREIGN KEY (ID_USER)
      REFERENCES USER (ID_USER) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PRODUCT_DETAIL ADD CONSTRAINT FK_RELATIONSHIP_16 FOREIGN KEY (ID_TYPE_DRESS)
      REFERENCES TYPE (ID_TYPE_DRESS) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PRODUCT_DETAIL ADD CONSTRAINT FK_RELATIONSHIP_4 FOREIGN KEY (PRODUCT_ARTICLE)
      REFERENCES PRODUCT (PRODUCT_ARTICLE) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PRODUCT_DETAIL ADD CONSTRAINT FK_RELATIONSHIP_5 FOREIGN KEY (ID_OPTION)
      REFERENCES �HARACTERISTICS (ID_OPTION) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PURCHASE ADD CONSTRAINT FK_RELATIONSHIP_12 FOREIGN KEY (ID_PROVIDER)
      REFERENCES PROVIDER (ID_PROVIDER) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE SIZE ADD CONSTRAINT FK_RELATIONSHIP_11 FOREIGN KEY (ID_SIZE_CATEGORY)
      REFERENCES CATEGORY_SIZE (ID_SIZE_CATEGORY) ON DELETE RESTRICT ON UPDATE RESTRICT;

