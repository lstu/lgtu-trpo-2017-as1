'use strict';

var url = require('url');

var Product = require('./ProductService');

module.exports.getProduct = function getProduct (req, res, next) {
  Product.getProduct(req.swagger.params, res, next);
};

module.exports.getProductSearch = function getProductSearch (req, res, next) {
  Product.getProductSearch(req.swagger.params, res, next);
};
