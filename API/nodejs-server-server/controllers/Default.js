'use strict';

var url = require('url');

var Default = require('./DefaultService');

module.exports.authPOST = function authPOST (req, res, next) {
  Default.authPOST(req.swagger.params, res, next);
};
