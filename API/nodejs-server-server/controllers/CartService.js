'use strict';

exports.addCart = function(args, res, next) {
  /**
   * add product in Cart
   * add product in Cart
   *
   * product_id Integer 
   * no response value expected for this operation
   **/
  res.end();
}

exports.buy products = function(args, res, next) {
  /**
   * Buy products
   * Buy products
   *
   * shipping_method String 
   * payment_method String 
   * country String 
   * city String 
   * street String 
   * house String 
   * flat Integer 
   * index String 
   * pick_point String 
   * no response value expected for this operation
   **/
  res.end();
}

exports.deleteCart = function(args, res, next) {
  /**
   * delete product in Cart
   * delete product in Cart
   *
   * product_id Integer 
   * no response value expected for this operation
   **/
  res.end();
}

exports.getUserCart = function(args, res, next) {
  /**
   * Cart
   * Information about Cart
   *
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "date" : "01-01-2017",
  "prodArt" : "1",
  "cost" : 213,
  "count" : 2
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

