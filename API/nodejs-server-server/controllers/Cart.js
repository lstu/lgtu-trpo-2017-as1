'use strict';

var url = require('url');

var Cart = require('./CartService');

module.exports.addCart = function addCart (req, res, next) {
  Cart.addCart(req.swagger.params, res, next);
};

module.exports.buy products = function buy products (req, res, next) {
  Cart.buy products(req.swagger.params, res, next);
};

module.exports.deleteCart = function deleteCart (req, res, next) {
  Cart.deleteCart(req.swagger.params, res, next);
};

module.exports.getUserCart = function getUserCart (req, res, next) {
  Cart.getUserCart(req.swagger.params, res, next);
};
