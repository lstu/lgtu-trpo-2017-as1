'use strict';

exports.getProduct = function(args, res, next) {
  /**
   * Product
   * Information about product
   *
   * product_id Integer 
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "size" : "M, L, S",
  "color" : "white",
  "material" : "cotton",
  "price" : 123,
  "name" : "Футболка",
  "description" : "Футболка черная",
  "brand" : "NewYorker",
  "article" : "123321",
  "picture" : "bin"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getProductSearch = function(args, res, next) {
  /**
   * ProductsSearch
   * Information about product after search return ID product
   *
   * product_name String 
   * product_brand String  (optional)
   * product_category String  (optional)
   * product_color String  (optional)
   * product_material String  (optional)
   * priceMax Integer  (optional)
   * priceMin Integer  (optional)
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "ID" : "1203",
  "category" : "Спортивная футболка"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

