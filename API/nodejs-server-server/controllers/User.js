'use strict';

var url = require('url');

var User = require('./UserService');

module.exports.getUser = function getUser (req, res, next) {
  User.getUser(req.swagger.params, res, next);
};
