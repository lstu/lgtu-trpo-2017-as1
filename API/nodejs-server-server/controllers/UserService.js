'use strict';

exports.getUser = function(args, res, next) {
  /**
   * User
   * Information about user
   *
   * user_id Integer userID
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "address" : "г. Липецк ул. Московкая 30",
  "phone" : "88005553555",
  "fio" : "Иванов Иван Иваныч",
  "userID" : "id123131",
  "email" : "ivan@ivan.com"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

