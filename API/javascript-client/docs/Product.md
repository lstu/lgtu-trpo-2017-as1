# Api.Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name of the product. | [optional] 
**article** | **String** | article of the product. | [optional] 
**brand** | **String** | brand of the product. | [optional] 
**size** | **String** | size of the product. | [optional] 
**color** | **String** | color of the product. | [optional] 
**material** | **String** | material of the product. | [optional] 
**picture** | **String** | picture of the product. | [optional] 
**description** | **String** | description of the product. | [optional] 
**price** | **Number** | price of the product. | [optional] 


