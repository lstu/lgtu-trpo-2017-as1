# Api.CartApi

All URIs are relative to *https://editor.swagger.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCart**](CartApi.md#addCart) | **POST** /cart/{product_id} | add product in Cart
[**buyProducts**](CartApi.md#buyProducts) | **POST** /cart/buy/ | Buy products
[**deleteCart**](CartApi.md#deleteCart) | **DELETE** /cart/{product_id} | delete product in Cart
[**getUserCart**](CartApi.md#getUserCart) | **GET** /cart | Cart


<a name="addCart"></a>
# **addCart**
> addCart(productId)

add product in Cart

add product in Cart

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.CartApi();

var productId = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.addCart(productId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="buyProducts"></a>
# **buyProducts**
> buyProducts(shippingMethod, paymentMethod, country, city, street, house, flat, index, pickPoint)

Buy products

Buy products

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.CartApi();

var shippingMethod = "shippingMethod_example"; // String | 

var paymentMethod = "paymentMethod_example"; // String | 

var country = "country_example"; // String | 

var city = "city_example"; // String | 

var street = "street_example"; // String | 

var house = "house_example"; // String | 

var flat = 56; // Number | 

var index = "index_example"; // String | 

var pickPoint = "pickPoint_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.buyProducts(shippingMethod, paymentMethod, country, city, street, house, flat, index, pickPoint, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shippingMethod** | **String**|  | 
 **paymentMethod** | **String**|  | 
 **country** | **String**|  | 
 **city** | **String**|  | 
 **street** | **String**|  | 
 **house** | **String**|  | 
 **flat** | **Number**|  | 
 **index** | **String**|  | 
 **pickPoint** | **String**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteCart"></a>
# **deleteCart**
> deleteCart(productId)

delete product in Cart

delete product in Cart

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.CartApi();

var productId = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.deleteCart(productId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserCart"></a>
# **getUserCart**
> [InlineResponse2003] getUserCart()

Cart

Information about Cart

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.CartApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getUserCart(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[InlineResponse2003]**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

