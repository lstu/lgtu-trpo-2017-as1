# Api.ProductsSearch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ID** | **String** | ID(article) of the product. | [optional] 
**category** | **String** | name of the product. | [optional] 


