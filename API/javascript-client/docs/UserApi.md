# Api.UserApi

All URIs are relative to *https://editor.swagger.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUser**](UserApi.md#getUser) | **GET** /users/{user_id} | User


<a name="getUser"></a>
# **getUser**
> [InlineResponse200] getUser(userId)

User

Information about user

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.UserApi();

var userId = 56; // Number | userID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getUser(userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Number**| userID | 

### Return type

[**[InlineResponse200]**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

