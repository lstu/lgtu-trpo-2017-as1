# Api.InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prodArt** | **String** | ID add product in the cart. | [optional] 
**_date** | **String** | date add product in the cart. | [optional] 
**cost** | **Number** | cost add product in the cart. | [optional] 
**count** | **Number** | count add product in the cart. | [optional] 


