# Api.InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fio** | **String** | fio of the user. | [optional] 
**address** | **String** | address of the user. | [optional] 
**phone** | **String** | phone of the user. | [optional] 
**email** | **String** | email address of the user | [optional] 
**userID** | **String** | email address of the user | [optional] 


