# Api.DefaultApi

All URIs are relative to *https://editor.swagger.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authPost**](DefaultApi.md#authPost) | **POST** /auth | Authentication


<a name="authPost"></a>
# **authPost**
> authPost(login, password)

Authentication

Returns API access token on successful authentication.

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.DefaultApi();

var login = "login_example"; // String | Login

var password = "password_example"; // String | md5 hash of password


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.authPost(login, password, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **String**| Login | 
 **password** | **String**| md5 hash of password | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

