# Api.ProductApi

All URIs are relative to *https://editor.swagger.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProduct**](ProductApi.md#getProduct) | **GET** /product/{product_id} | Product
[**getProductSearch**](ProductApi.md#getProductSearch) | **GET** /search | ProductsSearch


<a name="getProduct"></a>
# **getProduct**
> [InlineResponse2001] getProduct(productId)

Product

Information about product

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.ProductApi();

var productId = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getProduct(productId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 

### Return type

[**[InlineResponse2001]**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getProductSearch"></a>
# **getProductSearch**
> [InlineResponse2002] getProductSearch(productName, opts)

ProductsSearch

Information about product after search return ID product

### Example
```javascript
var Api = require('api');

var apiInstance = new Api.ProductApi();

var productName = "productName_example"; // String | 

var opts = { 
  'productBrand': "productBrand_example", // String | 
  'productCategory': "productCategory_example", // String | 
  'productColor': "productColor_example", // String | 
  'productMaterial': "productMaterial_example", // String | 
  'priceMax': 56, // Number | 
  'priceMin': 56 // Number | 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getProductSearch(productName, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productName** | **String**|  | 
 **productBrand** | **String**|  | [optional] 
 **productCategory** | **String**|  | [optional] 
 **productColor** | **String**|  | [optional] 
 **productMaterial** | **String**|  | [optional] 
 **priceMax** | **Number**|  | [optional] 
 **priceMin** | **Number**|  | [optional] 

### Return type

[**[InlineResponse2002]**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

