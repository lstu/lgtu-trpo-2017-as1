/**
 * API
 * All requests should have additional header \"token\" with token received by /auth endpoint.
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Cart', 'model/Error', 'model/InlineResponse200', 'model/InlineResponse2001', 'model/InlineResponse2002', 'model/InlineResponse2003', 'model/InlineResponseDefault', 'model/Product', 'model/ProductsSearch', 'model/User', 'api/CartApi', 'api/DefaultApi', 'api/ProductApi', 'api/UserApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./model/Cart'), require('./model/Error'), require('./model/InlineResponse200'), require('./model/InlineResponse2001'), require('./model/InlineResponse2002'), require('./model/InlineResponse2003'), require('./model/InlineResponseDefault'), require('./model/Product'), require('./model/ProductsSearch'), require('./model/User'), require('./api/CartApi'), require('./api/DefaultApi'), require('./api/ProductApi'), require('./api/UserApi'));
  }
}(function(ApiClient, Cart, Error, InlineResponse200, InlineResponse2001, InlineResponse2002, InlineResponse2003, InlineResponseDefault, Product, ProductsSearch, User, CartApi, DefaultApi, ProductApi, UserApi) {
  'use strict';

  /**
   * All_requests_should_have_additional_header_token_with_token_received_by_auth_endpoint_.<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var Api = require('index'); // See note below*.
   * var xxxSvc = new Api.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new Api.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
   * and put the application logic within the callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new Api.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new Api.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 0.0.1
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The Cart model constructor.
     * @property {module:model/Cart}
     */
    Cart: Cart,
    /**
     * The Error model constructor.
     * @property {module:model/Error}
     */
    Error: Error,
    /**
     * The InlineResponse200 model constructor.
     * @property {module:model/InlineResponse200}
     */
    InlineResponse200: InlineResponse200,
    /**
     * The InlineResponse2001 model constructor.
     * @property {module:model/InlineResponse2001}
     */
    InlineResponse2001: InlineResponse2001,
    /**
     * The InlineResponse2002 model constructor.
     * @property {module:model/InlineResponse2002}
     */
    InlineResponse2002: InlineResponse2002,
    /**
     * The InlineResponse2003 model constructor.
     * @property {module:model/InlineResponse2003}
     */
    InlineResponse2003: InlineResponse2003,
    /**
     * The InlineResponseDefault model constructor.
     * @property {module:model/InlineResponseDefault}
     */
    InlineResponseDefault: InlineResponseDefault,
    /**
     * The Product model constructor.
     * @property {module:model/Product}
     */
    Product: Product,
    /**
     * The ProductsSearch model constructor.
     * @property {module:model/ProductsSearch}
     */
    ProductsSearch: ProductsSearch,
    /**
     * The User model constructor.
     * @property {module:model/User}
     */
    User: User,
    /**
     * The CartApi service constructor.
     * @property {module:api/CartApi}
     */
    CartApi: CartApi,
    /**
     * The DefaultApi service constructor.
     * @property {module:api/DefaultApi}
     */
    DefaultApi: DefaultApi,
    /**
     * The ProductApi service constructor.
     * @property {module:api/ProductApi}
     */
    ProductApi: ProductApi,
    /**
     * The UserApi service constructor.
     * @property {module:api/UserApi}
     */
    UserApi: UserApi
  };

  return exports;
}));
